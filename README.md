# nomolo
vim nomolo theme (256 colors)

## Install
Install with your favourite package manager, e.g. `vim-plug`:

```
...
Plug 'https://gitlab.com/fankhauser/nomolo'
...
```

then load it with:

```
...
" theme
filetype plugin indent on
syntax enable
colorscheme nomolo
...
```
